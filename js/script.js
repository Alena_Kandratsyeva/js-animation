window.onload = function(){

    function Square (speed) {

        var animationStartTime = 0;
        var animationDuration = 1000;
        var target = document.querySelector('.square');

        this.startAnimationTimeOut = function (speed) {
            this.toStart();
            var start =Date.now();
            animationDuration = speed;
            setTimeout(function f () {
                var now = Date.now();
                var progress = (now - start)/ animationDuration;
                var result = ((now - start) / animationDuration)*540;
                target.style.transform = 'translate(' + result + 'px, 0px)';
                if (progress <= 1)
                    setTimeout(f, 10);
            }, 10);
        }

        this.startAnimationInterval = function(speed){
            this.toStart();
            animationDuration = speed;
            var start = Date.now();
            var timer = setInterval(function() {
                var now = Date.now();
                var progress = (now - start)/ animationDuration;
                var result = ((now - start) / animationDuration)*540;
                target.style.transform = 'translate(' + result + 'px, 0px)';
                if (progress > 1) clearInterval(timer);
            }, 10);
        }

        this.startAnimationFrame = function(speed) {
            animationDuration = speed;
            animationStartTime = Date.now();
            requestAnimationFrame(update);
        };

        this.toStart = function(){
            target.style.transform = 'translate(0px, 0px)';
        }

        function update() {
            var currentTime = Date.now();
            var positionInAnimation = (currentTime - animationStartTime) / animationDuration;
            var xPosition = positionInAnimation * 540;
            var yPosition = positionInAnimation * 1;
            target.style.transform = 'translate(' + xPosition + 'px, ' + yPosition + '0px)';
            if (positionInAnimation <= 1)
                requestAnimationFrame(update);
        }
    }

    var square = new Square();

    var speed = 1010 - Number(document.getElementById('slider').value)*10;

    function clear(event) {
        square.toStart();
    }

    var radios = document.getElementsByName("objType");
    for (var i = 0; i < radios.length; i++)
        radios[i].addEventListener("change", clear);

    slider.addEventListener('change', function () {
        speed = 0;
        speed = 1100 - Number(document.getElementById('slider').value)*10;
    }, false);

    move.onclick = function () {
        square.animationDuration = speed;
        var radios = document.getElementsByName("objType");
        for(var i = 0; i < radios.length; i++) {
            if(radios[i].checked) selectedValue = radios[i].value;
        }
        switch(selectedValue) {
            case 'setTimeout':
                square.startAnimationTimeOut(speed);
                break;
            case 'setInterval':
                square.startAnimationInterval(speed);
                break;
            default:
                square.startAnimationFrame(speed);
                break;
        }
    }
}
